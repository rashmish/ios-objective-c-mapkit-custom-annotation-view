//
//  ViewController.m
//  TempProject
//
//  Created by Kapil Sharma. on 3/10/16.
//  Copyright © 2020 Kapil Sharma. All rights reserved.
//

#import "ViewController.h"
#import "TempProject-Swift.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize mapView;

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
     NSMutableArray* annotations=[[NSMutableArray alloc] init];
  
           CLLocationCoordinate2D coord;
           coord.latitude = 29.484878;
           coord.longitude = 86.456734;
           Content *content = [Content new];
           content.coordinate = coord;
           NSDictionary *dict = @{ @"name" : @"Test", @"address" : @"Test addresss"};
           content.values = dict;
           [annotations addObject:content];

  [self.mapView addAnnotations:annotations];

}

#pragma mark- -------------------------------------------------------------
#pragma mark Map Delegates


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
  
    if ([annotation isKindOfClass: [MKUserLocation class]]) {
        return  nil;
        
    }
    static NSString *viewId = @"pin";
    MKAnnotationView * annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:viewId];
    if (annotationView == nil) {
        
        annotationView = [[AnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:viewId];
        annotationView.canShowCallout = NO;
       
        
    }else{
        annotationView.annotation = annotation;
    }
    annotationView.image = [UIImage imageNamed:@"marker_inactive"];
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
   if ([view isKindOfClass: [AnnotationView class]]) {
         for (UIView *subview in [view subviews]) {
             [subview removeFromSuperview];
         }
     }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass: [MKUserLocation class]]) {
              return ;
       }
       Content *counselorAnnotation = view.annotation;
       NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"CustomCalloutView"
         owner:self
       options:nil];
       CustomCalloutView *calloutView = [ nibViews objectAtIndex: 0];
     
  calloutView.userName.text = counselorAnnotation.values[@"name"];
       calloutView.userAddress.text = counselorAnnotation.values[@"address"];
  
  calloutView.btnShowProfile.tag = 1;
  [calloutView.btnShowProfile addTarget:self action: @selector(showProfileTap:) forControlEvents:UIControlEventTouchUpInside];

  calloutView.center =  CGPointMake(view.bounds.size.width / 2,-calloutView.bounds.size.height*0.52);
       [view addSubview:calloutView];
       [mapView setCenterCoordinate:view.annotation.coordinate animated:YES];

}

- (void)showProfileTap:(UIButton *)sender {
  NSLog(@"%ld", (long)sender.tag);
}

@end
