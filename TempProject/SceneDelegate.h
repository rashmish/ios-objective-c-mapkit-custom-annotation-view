//
//  SceneDelegate.h
//  TempProject
//
//  Created by Kapil Sharma. on 3/10/16.
//  Copyright © 2020 Kapil Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

