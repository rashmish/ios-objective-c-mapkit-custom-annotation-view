#import <MapKit/MapKit.h>

@interface Content : NSObject<MKAnnotation>

@property (nonatomic,retain) NSDictionary *values; // values to be applied on the calloutView instance
@property (nonatomic,assign) CLLocationCoordinate2D coordinate; // point of the annotation in the map

@end
