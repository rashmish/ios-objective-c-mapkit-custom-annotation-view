//
//  CustomCalloutView.swift
//  CustomCalloutView
//
//  Created by Kapil Sharma. on 3/10/16.
//  Copyright © 2020 Kapil Sharma. All rights reserved.
//

import UIKit

class CustomCalloutView: UIView {

    @IBOutlet var userImage: UIImageView!
  
    @IBOutlet var userName: UILabel!
    @IBOutlet var userAddress: UILabel!
  
  @IBOutlet var btnShowProfile: UIButton!
}
