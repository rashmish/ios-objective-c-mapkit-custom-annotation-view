//
//  ViewController.h
//  TempProject
//
//  Created by Kapil Sharma. on 3/10/16.
//  Copyright © 2020 Kapil Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Content.h"

@interface ViewController : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

